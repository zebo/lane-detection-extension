import numpy as np
import cv2


def canny(img, low_threshold, high_threshold):
    """Gaussian Blur for Image Denoising"""
    img = cv2.GaussianBlur(img, (3, 3), 0)

    """Applies the Canny transform"""
    return cv2.Canny(img, low_threshold, high_threshold)

def grayscale(img):
    """Applies the Grayscale transform
    This will return an image with only one color channel
    but NOTE: to see the returned image as grayscale
    you should call plt.imshow(gray, cmap='gray')"""
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def region_of_interest(img, vertices):
    """
    Applies an image mask.
    
    Only keeps the region of the image defined by the polygon
    formed from `vertices`. The rest of the image is set to black.
    """
    #defining a blank mask to start with
    mask = np.zeros_like(img)   
    
    #defining a 3 channel or 1 channel color to fill the mask with depending on the input image
    if len(img.shape) > 2:
        channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255
        
    #filling pixels inside the polygon defined by "vertices" with the fill color
    cv2.fillPoly(mask, vertices, ignore_mask_color)
    
    #returning the image only where mask pixels are nonzero
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image

def draw_lines(img, lines, color=[0, 255, 0], thickness=10):
    imshape = img.shape
    left_x1 = []
    left_x2 = []
    right_x1 = []
    right_x2 = [] 
    y_min = img.shape[0]
    y_max = int(img.shape[0]*0.611)
    for line in lines:
        for x1,y1,x2,y2 in line:
            if float(x2-x1)!=0 and (float(y2-y1)/float(x2-x1)) < 0:
                mc = np.polyfit([x1, x2], [y1, y2], 1)
                left_x1.append(np.int(np.float((y_min - mc[1]))/np.float(mc[0])))
                left_x2.append(np.int(np.float((y_max - mc[1]))/np.float(mc[0])))
#           cv2.line(img, (xone, imshape[0]), (xtwo, 330), color, thickness)
            elif float(x2-x1)!=0 and (float(y2-y1)/float(x2-x1)) > 0:
                mc = np.polyfit([x1, x2], [y1, y2], 1)
                right_x1.append(np.int(np.float((y_min - mc[1]))/np.float(mc[0])))
                right_x2.append(np.int(np.float((y_max - mc[1]))/np.float(mc[0])))
#           cv2.line(img, (xone, imshape[0]), (xtwo, 330), color, thickness)
    l_avg_x1 = np.int(np.nanmean(left_x1))
    l_avg_x2 = np.int(np.nanmean(left_x2))
    r_avg_x1 = np.int(np.nanmean(right_x1))
    r_avg_x2 = np.int(np.nanmean(right_x2))
#     print([l_avg_x1, l_avg_x2, r_avg_x1, r_avg_x2])
    cv2.line(img, (l_avg_x1, y_min), (l_avg_x2, y_max), color, thickness)
    cv2.line(img, (r_avg_x1, y_min), (r_avg_x2, y_max), color, thickness)    

def hough_lines(img, img_masked, rho, theta, threshold, min_line_len, max_line_gap):
    """
    `img_masked` should be the output of a Canny transform.
        
    Returns an image with hough lines drawn.
    """
    lines = cv2.HoughLinesP(img_masked, rho, theta, threshold, np.array([]), minLineLength=min_line_len, maxLineGap=max_line_gap)
    #line_img = np.zeros(tuple(img_masked.shape)+(3,), dtype=np.uint8)
    #print('lines',lines)
    #draw_lines(line_img, lines)
    #return line_img
    draw_lines(img, lines)
    return img

if __name__ == "__main__":
    img = cv2.imread('lane-raw.jpeg')

    # copy for addWeight later
    img_overlay = img.copy()

    height, width, channels = img.shape
    print('height, width, channels: ', height, width, channels)

    # grayscale and canny
    img_gs = grayscale(img_overlay)
    img_canny = canny(img_gs, 50, 150)

    # create vertexs of poly mask and mask image
    vertexs = [ [width*0.1,height], [width*0.47,height*0.6],
                [width*0.53,height*0.6], [width*0.9,height] ]
    roi_vertex = np.array(vertexs, np.int32)
    print('roi_vertex: ', roi_vertex)
    img_masked = region_of_interest(img_canny, [roi_vertex])

    # connect same direction lines and draw lines
    img_with_line = hough_lines(img_overlay, img_masked, 1.0, np.pi/180, 5, 1, 10)

    # make the shapes/lines transparent
    opacity = 0.4
    cv2.addWeighted(img_with_line, opacity, img, 1 - opacity, 0, img)

    # show and choose to save
    cv2.imshow('image',img)
    k = cv2.waitKey(0)
    if k == 27:         # wait for ESC key to exit
        cv2.destroyAllWindows()
    elif k == ord('s'): # wait for 's' key to save and exit
        cv2.imwrite('lane-ouput.png',img)
        cv2.destroyAllWindows()